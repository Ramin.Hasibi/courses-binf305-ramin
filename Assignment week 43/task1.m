c_prime = @(t,c) [-k(1)*c(1)*c(2) - k(3) *c(1)*c(3);
    -k(1)*c(1)*c(2) - k(2)*c(3)*c(2) + k(5)*c(5);
    k(1)*c(1)*c(2) - k(2)*c(2)*c(3) + k(3) *c(1)*c(3) - k(4)*c(3)*c(3);
    k(2)*c(3)*c(2) + k(4)*c(3)*c(3);
    k(3)*c(1)*c(3) - k(5)*c(5);
    ];
k = [ 1.34, 1.6e9 , 8.0e3 , 4.0e7 , 1.0 ] ;

options=odeset('reltol',1e-10,'abstol',1e-10);
timeinterval=[0.0, 200.0];
c0 = [ 0.6e-1, 0.33e-6,0.501e-10,0.3e-1,0.24e-7 ] ;
[t,c] = ode15s(c_prime, timeinterval, c0, options) ; %implicit solver for stiff systems
c = log(c)
figure(1)
hold on

plot(t,c(:,1),'linewidth',2)
plot(t,c(:,2),'linewidth',2)
plot(t,c(:,3),'linewidth',2)
plot(t,c(:,4),'linewidth',2)
plot(t,c(:,5),'linewidth',2)
xlabel('time')
ylabel('Production')
legend('A','B','C','D','E');
grid

timediff=t(2:end)-t(1:(end-1));
maxStepSize=max(timediff)
figure(2)
plot(t(2:end),timediff,'x')
xlabel('time')
ylabel('step size')


