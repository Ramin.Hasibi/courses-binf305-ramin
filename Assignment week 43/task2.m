clc;clear all; close all;
%parameters
p=zeros(1,4);
p(1)=0.05;        %k1
p(2)=0.7;         %k-1
p(3)=0.005;         %k2
p(4)=.4;       %K-2

tspan=[0,20];


[T,Y] = ode45(@testfun1,tspan,[1.5 3 2],[],p);

figure(1)
plot(T,Y(:,1),'k','linewidth',2);    %A
hold on
plot(T,Y(:,2),'r','linewidth',2);   %B
plot(T,Y(:,3),'o');   %C
% xlabel('time')
% ylabel('Production')
% legend('A','B','C','A-qssa','C-qssa');

xlabel('time')
ylabel('Production')

[T1,Y_QSSA] = ode45(@testfun2,tspan,[1.5 2],[],p);
 plot(T1,Y_QSSA(:,1),'b','linewidth',2);
plot(T1,Y_QSSA(:,2),'g','linewidth',2);

legend('A','B','C','A-qssa','C-qssa');
grid

Sqssa=interp1(T1,Y_QSSA,T);

figure(2)

plot(T,Y(:,3)-Sqssa(:,2))
xlabel('time')
ylabel('Difference between C and C\_QSSA')
figure(3)
plot(T,Y(:,1)-Sqssa(:,1))
xlabel('time')
ylabel('Difference between A and A\_QSSA')

function f=testfun1(t,y,p)
    f=zeros(3,1);
    f(1) = -p(1)*y(1) + p(2)*y(2);                
    f(2) = p(1)*y(1) - p(2)*y(2) - p(3)*y(2) + p(4)*y(3);                   
    f(3) = p(3)*y(2) - p(4)*y(3); 
end
function f=testfun2(t,y,p)
    f=zeros(2,1);
    f(1) = -p(1)*y(1) + (p(2)*((p(1)*y(1))+(p(4)*y(2)))/(p(2)+p(3)));                                 
    f(2) = (p(3)*((p(1)*y(1))+(p(4)*y(2)))/(p(2)+p(3))) - p(4)*y(2); 

end


